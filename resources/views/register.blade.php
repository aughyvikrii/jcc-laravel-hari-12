<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
    @csrf
        <label for="first_name">First name:</label> <br/>
        <input type="text" name="first_name"> <br/> <br/>
        
        <label for="last_name">Last name:</label> <br/>
        <input type="text" name="last_name"> <br/><br/>

        <label for="gender">Gender:</label> <br/>
        <input type="radio" name="gender" id="gender_male" value="male"> <label for="gender_male">Male</label> <br/>
        <input type="radio" name="gender" id="gender_female" value="female"> <label for="gender_female">Female</label> <br/>
        <input type="radio" name="gender" id="gender_other" value="other"> <label for="gender_other">Other</label> <br/> <br/>

        <label for="nationality">Nationality:</label> <br/>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="singapore">Singaporean</option>
            <option value="malaysia">Malaysian</option>
            <option value="australia">Australian</option>
        </select> <br/><br/>

        <label for="language">Language Spoken:</label> <br/>
        <input type="checkbox" name="language" id="lang_indonesia" value="indonesia"> <label for="lang_indonesia">Indonesia</label> <br/>
        <input type="checkbox" name="language" id="lang_english" value="english"> <label for="lang_english">English</label> <br/>
        <input type="checkbox" name="language" id="lang_other" value="other"> <label for="lang_other">Other</label> <br/> <br/> 

        <label for="bio">Bio:</label> <br/> 
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea> <br/> 

        <button type="submit">Sign Up</button>
    </form>
</body>
</html>